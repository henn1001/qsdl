/**
 * NOTE: This class was generated by QSDL.
 */
package app.server.service;

import app.server.domain.*;
import app.server.domain.entity.*;
import app.server.domain.mapper.*;
import app.server.exception.AppException;
import app.server.model.*;
import app.server.repository.*;
import app.server.util.PredicateBuilder;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jakarta.annotation.PostConstruct;

import java.util.*;

@Slf4j
@Service
public class TicketService {

  @Autowired
  private TicketRepository ticketRepository;

  @Autowired
  private TicketMapStruct ticketMapStruct;

  @PostConstruct
  private void init() {
    // Executed after dependency injection.
  }

  private TicketEntity fetchTicketFromDb(Long id) throws AppException {
    return ticketRepository.findById(id)
        .orElseThrow(() -> AppException.entityNotFound(Ticket.class, id));
  }

  public CursorPage<Ticket> getTickets(CursorPageable pageable, Context context) throws AppException {

    var queryParameters = Arrays.<String>asList();
    var predicate = PredicateBuilder.build(context.getParameterMap(queryParameters), TicketEntity.class);

    var cursorPage = ticketRepository.findAll(predicate, pageable);

    var ticketEntities = cursorPage.items();
    var ticketDtos = ticketEntities.stream().map(ticketMapStruct::toDto).toList();

    return new CursorPage<Ticket>(ticketDtos, cursorPage.nextCursor(), cursorPage.totalCount());
  }

  @Transactional
  public Ticket createTicket(Ticket body, Context context) throws AppException {

    var ticketEntity = ticketMapStruct.toEntity(body);

    ticketEntity = ticketRepository.save(ticketEntity);

    return ticketMapStruct.toDto(ticketEntity);
  }

  public Ticket getTicket(Long id, Context context) throws AppException {

    var ticketEntity = fetchTicketFromDb(id);

    return ticketMapStruct.toDto(ticketEntity);
  }

  @Transactional
  public Ticket replaceTicket(Long id, Ticket body, Context context) throws AppException {

    var ticketEntity = fetchTicketFromDb(id);

    // replace ticketEntity with all writeable fields - nulls included
    ticketMapStruct.replace(body, ticketEntity);

    ticketEntity = ticketRepository.save(ticketEntity);

    return ticketMapStruct.toDto(ticketEntity);
  }

  @Transactional
  public Ticket updateTicket(Long id, Ticket body, Context context) throws AppException {

    var ticketEntity = fetchTicketFromDb(id);

    // update dbEntity with all writeable fields if present
    ticketMapStruct.update(body, ticketEntity);

    ticketEntity = ticketRepository.save(ticketEntity);

    return ticketMapStruct.toDto(ticketEntity);
  }

  @Transactional
  public Void deleteTicket(Long id, Context context) throws AppException {

    var ticketEntity = fetchTicketFromDb(id);

    ticketRepository.delete(ticketEntity);

    return null;
  }

}
