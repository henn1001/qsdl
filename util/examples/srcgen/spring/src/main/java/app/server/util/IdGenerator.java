/**
 * NOTE: This class was generated by QSDL.
 */
package app.server.util;

import java.util.UUID;

public class IdGenerator {

  public static String createId() {
    UUID uuid = UUID.randomUUID();
    return uuid.toString();
  }

}
