/**
 * NOTE: This class was generated by QSDL.
 */
package app.server.constant;

import java.util.EnumSet;

public enum Status {

  OPEN,
  TO_DO,
  CLOSED;

  public static boolean hasValue(String value) {
    return EnumSet.allOf(Status.class)
        .stream()
        .anyMatch(e -> e.toString().equals(value));
  }

}
