/**
 * NOTE: This class was generated by QSDL.
 */
package app.server.service;

import app.server.domain.*;
import app.server.domain.entity.*;
import app.server.domain.mapper.*;
import app.server.exception.AppException;
import app.server.model.*;
import app.server.repository.*;
import app.server.util.PredicateBuilder;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jakarta.annotation.PostConstruct;

import java.util.*;

@Slf4j
@Service
public class ProjectService {

  @Autowired
  private ProjectRepository projectRepository;

  @Autowired
  private ProjectMapStruct projectMapStruct;

  @PostConstruct
  private void init() {
    // Executed after dependency injection.
  }

  private ProjectEntity fetchProjectFromDb(Long id) throws AppException {
    return projectRepository.findById(id)
        .orElseThrow(() -> AppException.entityNotFound(Project.class, id));
  }

  public CursorPage<Project> getProjects(CursorPageable pageable, Context context) throws AppException {

    var queryParameters = Arrays.<String>asList("name");
    var predicate = PredicateBuilder.build(context.getParameterMap(queryParameters), ProjectEntity.class);

    var cursorPage = projectRepository.findAll(predicate, pageable);

    var projectEntities = cursorPage.items();
    var projectDtos = projectEntities.stream().map(projectMapStruct::toDto).toList();

    return new CursorPage<Project>(projectDtos, cursorPage.nextCursor(), cursorPage.totalCount());
  }

  @Transactional
  public Project createProject(Project body, Context context) throws AppException {

    var projectEntity = projectMapStruct.toEntity(body);

    projectEntity = projectRepository.save(projectEntity);

    return projectMapStruct.toDto(projectEntity);
  }

  public Project getProject(Long id, Context context) throws AppException {

    var projectEntity = fetchProjectFromDb(id);

    return projectMapStruct.toDto(projectEntity);
  }

  @Transactional
  public Project replaceProject(Long id, Project body, Context context) throws AppException {

    var projectEntity = fetchProjectFromDb(id);

    // replace projectEntity with all writeable fields - nulls included
    projectMapStruct.replace(body, projectEntity);

    projectEntity = projectRepository.save(projectEntity);

    return projectMapStruct.toDto(projectEntity);
  }

  @Transactional
  public Project updateProject(Long id, Project body, Context context) throws AppException {

    var projectEntity = fetchProjectFromDb(id);

    // update dbEntity with all writeable fields if present
    projectMapStruct.update(body, projectEntity);

    projectEntity = projectRepository.save(projectEntity);

    return projectMapStruct.toDto(projectEntity);
  }

  @Transactional
  public Void deleteProject(Long id, Context context) throws AppException {

    var projectEntity = fetchProjectFromDb(id);

    projectRepository.delete(projectEntity);

    return null;
  }

}
