/**
 * NOTE: This class was generated by QSDL.
 */
package app.server.service;

import app.server.domain.*;
import app.server.domain.entity.*;
import app.server.domain.mapper.*;
import app.server.exception.AppException;
import app.server.model.*;
import app.server.repository.*;
import app.server.util.PredicateBuilder;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jakarta.annotation.PostConstruct;

import java.util.*;

@Slf4j
@Service
public class UserService {

  @Autowired
  private TicketRepository ticketRepository;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private UserMapStruct userMapStruct;

  @PostConstruct
  private void init() {
    // Executed after dependency injection.
  }

  private UserEntity fetchUserFromDb(Long id) throws AppException {
    return userRepository.findById(id)
        .orElseThrow(() -> AppException.entityNotFound(User.class, id));
  }

  private UserEntity fetchUserFromTicketFromDb(Long ticketId, Long id) throws AppException {
    return userRepository.findByTicketsIdAndId(ticketId, id)
        .orElseThrow(() -> AppException.entityNotFound(User.class, id));
  }

  private TicketEntity fetchTicketFromDb(Long id) throws AppException {
    return ticketRepository.findById(id)
        .orElseThrow(() -> AppException.entityNotFound(Ticket.class, id));
  }

  public CursorPage<User> getUsersForTicket(Long ticketId, CursorPageable pageable, Context context) throws AppException {

    // confirm existence of parent
    // should be optimized with something like getReferenceById
    var ticketEntity = fetchTicketFromDb(ticketId);

    var queryParameters = Arrays.<String>asList();
    var predicate = PredicateBuilder.build(context.getParameterMap(queryParameters), UserEntity.class);
    predicate.and(QUserEntity.userEntity.tickets.any().id.eq(ticketEntity.getId()));

    var cursorPage = userRepository.findAll(predicate, pageable);

    var userEntities = cursorPage.items();
    var userDtos = userEntities.stream().map(userMapStruct::toDto).toList();

    return new CursorPage<User>(userDtos, cursorPage.nextCursor(), cursorPage.totalCount());
  }

  @Transactional
  public Void addUserToTicket(Long ticketId, Long id, Context context) throws AppException {

    // get and confirm existence
    var ticketEntity = fetchTicketFromDb(ticketId);

    var userEntity = fetchUserFromDb(id);

    if (userEntity.tickets.contains(ticketEntity)) {
      throw AppException.entityAlreadyAdded(User.class, id);
    }

    userEntity.addToTickets(ticketEntity);

    userRepository.save(userEntity);

    return null;
  }

  @Transactional
  public Void removeUserFromTicket(Long ticketId, Long id, Context context) throws AppException {

    // get and confirm existence
    var ticketEntity = fetchTicketFromDb(ticketId);

    var userEntity = fetchUserFromTicketFromDb(ticketEntity.getId(), id);

    userEntity.removeFromTickets(ticketEntity);

    userRepository.save(userEntity);

    return null;
  }

  public CursorPage<User> getUsers(CursorPageable pageable, Context context) throws AppException {

    var queryParameters = Arrays.<String>asList();
    var predicate = PredicateBuilder.build(context.getParameterMap(queryParameters), UserEntity.class);

    var cursorPage = userRepository.findAll(predicate, pageable);

    var userEntities = cursorPage.items();
    var userDtos = userEntities.stream().map(userMapStruct::toDto).toList();

    return new CursorPage<User>(userDtos, cursorPage.nextCursor(), cursorPage.totalCount());
  }

  @Transactional
  public User createUser(User body, Context context) throws AppException {

    var userEntity = userMapStruct.toEntity(body);

    userEntity = userRepository.save(userEntity);

    return userMapStruct.toDto(userEntity);
  }

  public User getUser(Long id, Context context) throws AppException {

    var userEntity = fetchUserFromDb(id);

    return userMapStruct.toDto(userEntity);
  }

  @Transactional
  public User replaceUser(Long id, User body, Context context) throws AppException {

    var userEntity = fetchUserFromDb(id);

    // replace userEntity with all writeable fields - nulls included
    userMapStruct.replace(body, userEntity);

    userEntity = userRepository.save(userEntity);

    return userMapStruct.toDto(userEntity);
  }

  @Transactional
  public User updateUser(Long id, User body, Context context) throws AppException {

    var userEntity = fetchUserFromDb(id);

    // update dbEntity with all writeable fields if present
    userMapStruct.update(body, userEntity);

    userEntity = userRepository.save(userEntity);

    return userMapStruct.toDto(userEntity);
  }

  @Transactional
  public Void deleteUser(Long id, Context context) throws AppException {

    var userEntity = fetchUserFromDb(id);

    userRepository.delete(userEntity);

    return null;
  }

}
