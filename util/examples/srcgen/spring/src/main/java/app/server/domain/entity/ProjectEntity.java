/**
 * NOTE: This class was generated by QSDL.
 */
package app.server.domain.entity;

import app.server.model.AbstractPersistentObject;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.node.ObjectNode;

import jakarta.persistence.*;
import jakarta.validation.constraints.*;

import java.time.*;
import java.util.*;

@Entity
@Table(name = "project")
public class ProjectEntity extends AbstractPersistentObject {

  @NotNull
  public String name;

  public String description;

  public String creationBy;

  public LocalDate creationDate;

  public String lastUpdateBy;

  public OffsetDateTime lastUpdateDate;

  @Convert(converter = app.server.util.NodeConverter.class)
  @Column(columnDefinition = "text")
  public ObjectNode metaInf;

  public Boolean archive;

  @JsonIgnore
  @OneToMany(mappedBy = "project", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  public final Set<RoleEntity> roles = new LinkedHashSet<>();


  public void addToRoles(RoleEntity o) {
    o.project = this;
    this.roles.add(o);
  }

  public void removeFromRoles(RoleEntity o) {
    o.project = null;
    this.roles.remove(o);
  }

}
