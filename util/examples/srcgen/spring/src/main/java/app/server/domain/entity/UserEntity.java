/**
 * NOTE: This class was generated by QSDL.
 */
package app.server.domain.entity;

import app.server.model.AbstractPersistentObject;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.*;
import jakarta.validation.constraints.*;

import java.util.*;

@Entity
@Table(name = "user")
public class UserEntity extends AbstractPersistentObject {

  @NotNull
  public String name;

  public Integer count;

  @Column(unique = true)
  public String mail;

  @JsonIgnore
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "ticket_to_user", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "ticket_id"))
  public final Set<TicketEntity> tickets = new LinkedHashSet<>();


  public void addToTickets(TicketEntity o) {
    o.users.add(this);
    this.tickets.add(o);
  }

  public void removeFromTickets(TicketEntity o) {
    o.users.remove(this);
    this.tickets.remove(o);
  }

}
