# SpringBootApp

## Overview


### Features

* * *

## Requirements

-   [OpenJDK 17](https://adoptium.net/)

## Running the application

Running the service(s) is as simple as starting the jar file:

    java -jar app.jar

## Configuration

* * *

The following configuration parameters are available.


-   `--server.port` (Optional) Used port for this Service. The default is `8080`.
