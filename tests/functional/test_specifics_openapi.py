# Copyright (C) 2022 henn1001

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from tests import wrapper_generate, wrapper_generate_failure


class TestSpecificsOpenAPI:
    """Test specific OpenAPI functionality.

    01. The usage of a `Base` on any `Field` value creates a nested JSON Object.

    02. The usage of a `Object` on any `Field` value creates a nested JSON Object.

    03. `Directive` @namespace must use `PascalCase`.

    """

    def test_specifics_01_positive(self):
        """Verify usage of base reference"""
        test_input = """\
            base Foo {
                field1: Bar
            }

            base Bar {
                name: String
            }

            type Fruit extends Foo {
                field2: [Bar]
            }
        """

        openapi = wrapper_generate(test_input)

        properties = openapi["components"]["schemas"]["Fruit"]["properties"]

        assert properties["field1"]["$ref"]
        assert properties["field2"]["items"]["$ref"]

    def test_specifics_02_positive(self):
        """Verify usage of type reference"""
        test_input = """\
            base Foo {
                field1: Bar
            }

            type Bar {
                name: String
            }

            type Fruit extends Foo {
                field2: [Bar]
            }
        """

        openapi = wrapper_generate(test_input)

        properties = openapi["components"]["schemas"]["Fruit"]["properties"]

        assert properties["field1"]["$ref"]
        assert properties["field2"]["items"]["$ref"]

    def test_specifics_03_positive(self):
        """Verify PascalCase naming convention"""
        test_input = """\
            type Foo @namespace("Test") {
                field: String
            }
        """

        wrapper_generate(test_input)

    def test_specifics_03_negative(self):
        """Verify PascalCase naming convention"""
        inputs = []

        inputs.append('type Foo @namespace("wrong") { field: String } ')
        inputs.append('type Foo @namespace("Wro-Ng") { field: String } ')
        inputs.append('type Foo @namespace("WRO_NG") { field: String } ')

        for test_input in inputs:
            wrapper_generate_failure(test_input)

    def test_specifics_04_positive(self):
        """Verify usage of relations without parent endpoints"""
        test_input = """\
            type Foo {
                field1: String
            }

            type Bar {
                name: String
                foos: [Foo] @aggregation

                extend api {    }
            }

            type Fruit  {
                name: String
                foos: [Foo] @composition

                extend api {    }
            }

        """

        openapi = wrapper_generate(test_input)

        schemas = openapi["components"]["schemas"]
        assert schemas["FooList"]
        assert schemas["Foo"]
        assert schemas["Bar"]
        assert schemas["Fruit"]

        paths = openapi["paths"]
        assert paths["/bars/{bar_id}/foos"]
        assert paths["/fruits/{fruit_id}/foos"]
        assert "/bars" not in paths
        assert "/bars" not in paths
