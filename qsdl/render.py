# Copyright (C) 2022 henn1001

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Jinja2 render unit"""

from pathlib import Path

import jinja2

import qsdl.filter as qfilter
from qsdl import logger

log = logger.getLogger(__name__)


def render(
    output_file: Path,
    context: dict,
    template_path: Path,
    macro_path: Path = None,
    type_name: str = None,
    type_def: object = None,
):
    """Pass the python object graph to jinja for template rendering.

    Args:
        output_file (Path): The output path.
        context (dict): The context for jinja template.
        template_path (Path): The path to the jinja template.
        macro_path (Path, optional): [description]. Defaults to None.
        type_name (str, optional): [description]. Defaults to None.
        type_def (object, optional): [description]. Defaults to None.
    """

    # initialize the template engine.
    loaders = []

    loaders.append(jinja2.FileSystemLoader(template_path.parent))

    if macro_path:
        loaders.append(jinja2.FileSystemLoader(macro_path.parent))

    loader = jinja2.ChoiceLoader(loaders)

    jinja_env = jinja2.Environment(loader=loader, trim_blocks=True, lstrip_blocks=True)

    # register the filter for mapping Entity type names to type defs.
    if type_name and type_def:
        jinja_env.filters[type_name] = type_def

    jinja_env.filters["pluralize"] = qfilter.pluralize
    jinja_env.filters["pascal"] = qfilter.pascalcase
    jinja_env.filters["camel"] = qfilter.camelcase
    jinja_env.filters["snake"] = qfilter.snakecase
    jinja_env.filters["spinal"] = qfilter.spinalcase
    jinja_env.filters["regex_replace"] = qfilter.regex_replace

    # load the template
    template = jinja_env.get_template(template_path.name)

    # generate folders if needed
    output_file.parent.mkdir(exist_ok=True, parents=True)

    # generate code
    log.info("rendering file: %s", output_file)
    tmp = template.render(context)

    with open(output_file, "w", encoding="utf-8") as file:
        file.write(tmp)
